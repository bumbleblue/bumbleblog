### What is this?

Personal website of Eileen Wagner.

Using the Eleventy Netlify Boilerplate by [Dan Urbanowicz](https://www.danurbanowicz.com/)

### Run Eleventy (builds the site)

```
npx eleventy --serve
```

Or build automatically when a template changes:
```
npx eleventy --watch
```

Or in debug mode:
```
DEBUG=* npx eleventy
```
