---
layout: layouts/home.njk
title: eileen wagner
date: 2017-01-01T00:00:00.000Z
permalink: /
navtitle: who is
tags:
  - nav
---

I help people make sense of complex technologies. This involves thinking and reworking and exploring and testing. I deliver maps and diagrams. My areas of expertise are information security, distributed networking, open source design, digital infrastructure.

## now

* developing local-first ux
* maintaining a [pattern library for distributed systems](https://decentpatterns.com/)
* consulting on design x public interest tech
* pondering whether concrete abstract objects exist
