---
title: outputs
layout: layouts/blog.njk
date: 2018-01-01T00:00:00.000Z
permalink: /outputs/
navtitle: outputs
tags:
  - nav
---

## writing

<a href='https://www.inkandswitch.com/upwelling/'>upwelling</a>, UX for a local-first collaborative text editor
my case study on <a href='https://simplysecure.org/resources/case_studies/NoScript_CaseStudy_2019.pdf'>NoScript</a>
my master’s thesis on <a href='https://www.illc.uva.nl/Research/Publications/Reports/MoL-2015-23.text.pdf'>Superplural Logic</a>

## talks and workshops

design FAQs at [FOSS Backstage](https://youtu.be/NTB3qTf_FNU) 2021
towards a common glossary for decentral technologies at [Our Networks](https://ournetworks.ca/program/#towards-a-common-glossary-for-decentralized) 2020
open source design at [DWebDesign Berlin](https://jolocom.io/blog/open-source-design-resources/) 2019
designing for security at [Think About!](https://think-about.io/events/2019/speakies/eileen_wagner.html) 2019
communicating privacy in open networks at [Radical Networks](https://radicalnetworks.org/archives/2018/participants/eileen-wagner/) 2018
access and accessibility at [TEDxHeidelberg](https://youtu.be/lM0U0i2TNkQ) 2017

## snippets