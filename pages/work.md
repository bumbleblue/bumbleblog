---
title: work with me
date: 2018-01-01T00:00:00.000Z
permalink: /work/
navtitle: work with me
tags:
  - nav
---
I’m available for projects and consultations.

I can help you scope a UX project and define deliverables. I work best in a cross-functional team, and you'll typically get the most value pairing me with someone on your team. I am based in Berlin, but open to travel inside Europe, and happy working remotely. English and German are my working languages. My service offer covers most parts of the design process.

### Research

- Planning, and executing qualitative user interviews                        
- Identifying deep, core issues as well as superficial complaints or compliments
- Training teams to conduct their own guerrilla research
- Delivering a clear, concise, supported, and action-oriented research report

<pre>METHODS - stakeholder mapping • heuristic reviews • interviews and focus groups • analytics/synthesis • systems diagrams • journey mapping • personas • surveys • consequence scanning</pre>

###  Design                              

- Generating rough concept sketches
- Iterating on wireframes for interfaces and architectures
- Testing ideas and prototypes with users and stakeholders                        
- Creating external or internal process documentation                        
- Ongoing support on implementation

<pre>METHODS - requirements documents • conceptual engineering • interaction design • storyboards • application flows • IA site maps • wireframes • click-through prototypes • copywriting • design sprints</pre>

###  Everything Else

- Liaising between development and design teams
- Filing #ux Github issues                        
- Mentoring newcomers to UX design
- Facilitating hard conversations   
           
<pre>METHODS - meticulous note-taking • gentle hand-holding • creating new post-it note color palettes • doing <a href="https://www.youtube.com/watch?v=9EilqfAIudI">the ball thing</a> every now and then</pre>

Consultations can be regular or one-off meetings to discuss a focused problem. You can request a consultation directly.

<a href="https://cal.com/bumbleblue/consultation"><button>request a consultation</button></a>