---
title: Geach on Meaning
summary: a transcription
date: 2014-01-20
tags:
  - post
  - music
  - logic
---
*I was very sad to hear that Peter Geach died recently. Last year, Leiter Reports posted a [recording](http://leiterreports.typepad.com/blog/2013/05/geach-sings-about-frege-russell-and-wittgenstein-.html) of Geach singing his version of the Lorelei (written by Heinrich Heine, set to music by Friedrich Silcher). I transcribed the German lyrics for those who are curious.*

<pre>
Ich weiß nicht, was soll es bedeuten
Dass ich so traurig bin
Denn Frege auf manchen Seiten
Erklärte es sei doch ein Sinn
Die Angst wirkt schwer und es dunkelt
Und nichtet, nichtet das Nein
Das gold’ne Gebirge funkelt
Im tiefsten Außersein

Ein rundes Viereck blitzet
Darüber wunderbar
Der König von Frankreich sitzet
Und kämmt sein einziges Haar
Mit einem einzahnigen Kamme
Und singt ein Lied dabei
Das hat eine ganz seltsame
Störspannende Melodei

Der Frege im kleinen Schiffe
Erhört es mit wildem Weh
Er sieht nicht die Widerspruchsriffe
Er starrt nur hinauf in die Höh’
Ich glaube die Wellen verschlingen
Am Ende Frege und Kahn
Und das hat mit seinem Singen
Herr Wittgenstein getan!
</pre>
