---
title: Introducing Numeric Form
summary: simplifying barbershop music notation
date: 2019-10-06
tags:
  - post
  - music
  - community
---

I'm proposing a key-agnostic way of looking at short pieces of barbershop music (ahem, otherwise known as [tags](https://www.barbershoptags.com/)!). It maps scale degrees to numbers, so "do re mi fa sol la ti do" in Solfege becomes "1 2 3 4 5 6 7 1". To indicate whether it's the higher or lower scale degree, I am adding dots: one above means one octave above, two above means two octaves above, and same with dots below. These are understood in *absolute* terms, i.e. the octave relative to the key, not relative to the previous octave. A "`|`" stands for the end of a measure, and "`-`" and "`–`" are rudimentary rhythm symbols. So a tag might look like this:

```
|  3  5  4  -  |  4  3  —  |
|  1  1  1  7̣  |  7̣  2  1  |
|  5̣  5̣  5̣  -  |  5̣  5̣  —  |
|  1̣  3̣  2̣  -  |  2̣  1̣  —  |
Close your eyes,  in sleep.
```

As with Barbershop music, the order of the voices (from top row to bottom row) is tenor, lead, baritone, bass. To keep it minimal, I would add one letter at the top to show the key the tag was originally written in (or at least the recommended key). 

Of course, it wouldn't be barbershop without some flats and sharps, and that would look like this:

```
1=G
|  1  3  5  |  4  6  -  1̇  |  1̇  —  |
|  1  1 ♯1  |  2 ხ3  -  4  |  3  —  |
|  1 ხ7̣  6̣  |  1  1  - ხ6̣  |  5̣  —  |
|  1  5̣  3̣  |  6̣  4̣  -  2̣  |  1̣  —  |
So tired of   wait-ing for  you.
```

This notation does not allow for much complexity, but it would be nice to do some simple rhythms. Think of "`·`" as a dotted note, and "`_`" as the opposite. A "`⁀`" remains a tie. Here's what that might look like:

```
1=F
|  1̇  3̇· 2̲  |  1̇  1̇  7  |  1̇  1̇  2̇  |  1̇  –  |
|  5  1̇· 7̲  |  6 ხ6 Ⴉ6  |  5 ♯4 Ⴉ4  |  3  –  |
|  3  5·♯5̲  |  3  2 ♯2  |  3  6̣  7̣  |  5̣  –  |
|  1  1· 3̣̲  |  6̣  4̣ ♯4̣  |  5̣  2̣  5̣  |  1̣  –  |
Ire-land my Ire-land, I’m long-ing for  you.
```

Compare that to the original notation.

![](/static/img/Ireland_tag.png)

Taking all of the items together, we've just created the following semantics:

| Symbol | Meaning |
|:------|:--------|
| `3            ` | third scale degree or three or mi in Solfege |
| `ხ3` | flat three | 
| `♯5` | sharp five | 
| `Ⴉ3` | natural three  | 
| `|` | measure end | 
| `1̲` | half note (remove one half) | 
| `·` | dotted note (add one half) | 
| `2⁀2` | tie | 
| `-` | hold quarter note |
| `–` | hold half note |

### Who is it for?

Numeric form is mainly designed for quick look-ups. It's compact and key-agnostic, but not everyone will be comfortable reading music in Solfege. So first and foremost, this notation is great for tag teachers. For everyone else, tags in numeric form seem either very intuitive or very confusing, so use at your own risk!

### Where does it come from?

I've seen notation in this form in Chinese songbooks, but for melodies only. Take Richie Ren's 1996 classic, Xin Tai Ruan, and here's the whole song in what is called "simple sheet music" ("简谱" in Chinese).

![](/static/img/xin_tai_ruan.png)

Pretty neat, huh?

### DIY

You can write tags in numeric form, too! The key to making it all look nice is using a monospace font so that it all aligns beautifully. Beyond that, it's just some lightweight Unicode hacking. 

Of course, &#9837; and &#9839; and &#9838; all exist in Unicode already. But their spacing is weird, and only ♯ aligns well vertically. So instead I chose the Georgian letter xan (ხ) for flats, and another Georgian capital letter kan (Ⴉ) for naturals. This can be improved in the future!

For those willing to experiment, I'm listing all the characters I'm using along with the html codes that go with them. Note that if you're using the dot above, you'll want a typeface that is a little lower, such as Menlo or Monaco; otherwise it won't display correctly. I'm using [Pitch](https://klim.co.nz/retail-fonts/pitch/) here, appropriately.

```
1̇  > 1&#775; |   1̈  > 1&#776; |  
2̇  > 2&#775; |   2̈  > 2&#776; |  
3̇  > 3&#775; |   3̈  > 3&#776; |  
4̇  > 4&#775; |   4̈  > 4&#776; |  
5̇  > 5&#775; |   5̈  > 5&#776; |  
6̇  > 6&#775; |   6̈  > 6&#776; |  
7̇  > 7&#775; |   7̈  > 7&#776; |  
   
1̣  > 1&#803; |   1̤  > 1&#804; |  
2̣  > 2&#803; |   2̤  > 2&#804; |  
3̣  > 3&#803; |   3̤  > 3&#804; |  
4̣  > 4&#803; |   4̤  > 4&#804; |  
5̣  > 5&#803; |   5̤  > 5&#804; |  
6̣  > 6&#803; |   6̤  > 6&#804; |  
7̣  > 7&#803; |   7̤  > 7&#804; |  
   
1̲  > 1&#818; |   ხ  > &#4334; |
2̲  > 2&#818; |   Ⴉ  > &#4265; |
3̲  > 3&#818; |   ♯  > &#9839; |
4̲  > 4&#818; |   ·  > &#183;  |
5̲  > 5&#818; |   ⁀  > &#8256; |
6̲  > 6&#818; |
7̲  > 7&#818; |

```

### Possible next steps

*  put more tags in numeric form!
*  create a repository where you can download them as a Markdown file
*  design a LaTeX template that displays them nicely (Markdown > pandoc > LaTeX)
*  put out a first PDF release of "favourite tags in numeric form"

Feedback, ideas? Write me!

*One more tag!*

```
1=C
|  1  2  |  3  3  3  2  | ხ3  2  | Ⴉ3  —  |
|  1  2  |  1  1  1  -  |  1  —  |  1  —  |
|  1  7̣  |  6̣  6̣  6̣  -  | ხ6̣  5̣  |  5̣  —  |
|  1  7̣  |  6̣  5̣ ♯4̣  -  | Ⴉ4̣  —  |  1̣  —  |
When it’s slee-py time   down -   south.  
```
