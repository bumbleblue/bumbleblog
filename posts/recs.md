---
title: Pointers
summary: some recurring product recommendations
date: 2025-01-04
tags:
  - post
  - design
---

I’m putting together some recurring recommendations. Obviously, no affiliates or commissions here, just a quick note to save some copy/pasting.

### Suitcase

Samsonite Essens line
https://shop.samsonite.com/collections/essens/

Hard-shell suitcases often suffer from two problems: zippers are a weak point, and once you open them they take a lot of floor space. This Samsonite has a more classic design for polyethylene suitcases with clasps, it’s made out of lightweight recycled materials and seems to hold its shape well after a year of regular traveling. What’s more, the packing cubes inside make it easy to pack and unpack the suitcase; I often put them directly into drawers.


### Notebook

Moo Hardcover Notebook
https://www.moo.com/us/notebooks-journals/hardcover

Ever want a notebook that actually lies flat? This is it. The paper quality is luxe, and the coloured pages in the middle is a nice add-on.


### Pens

Paper Mate Flair Felt Tip Pens, Medium Point (0.7mm)
https://www.papermate.com/pens/felt-tip-pens/paper-mate-flair-felt-tip-pens-medium-point-0.7mm/SAP_8432452PP.html

A joy to write with, also looks good on sticky notes.


### Backpack

ULA Dragonfly
https://www.ula-equipment.com/product/dragonfly/

Lightweight and versatile backpack, great access and adjustment options.


### Purse

Tom Bihn Side Effect
https://www.tombihn.com/collections/best-sellers/products/side-effect

If you know me, you know that Tom Bihn is my design north star. This little everyday pack is virtually indestructible, can be carried across the shoulder or as a fanny pack, and uses the O-ring system—now also adopted by other bag makers. There are many awesome things about this pack, but the one thing I come to appreciate over and over again is the double zipper on the top of the pack, rather than the front. 


### Art

The Public Domain Review
https://publicdomainreview.org/

A quirky collection of artefacts in the public domain.


### Knives

Pallarès Solsona Carbon Steel Knives
https://www.solsona-knives.com/carbon-classic

These high-quality knives from a Catalan family business are affordable and durable. Yes, you need to dry them after every use. But edge retention!


### Pressure cooker

Kuhn Rikon DUROMATIC® INOX
https://kuhnrikon.com/de/blog/post/duromatic-inox

Much simpler than an Instant Pot, this pressure cooker can sear things on a stove before your quick-cook it. Cleaning is straightforward and parts are replaceable.


### Coffee grinder

Option-O LAGOM mini
https://www.option-o.com/lagom-mini

Small, powerful, does what it promises.


### Plates

Schönwald 898
https://www.bhs-tabletop.com/en-en/product/form-898-plate-flat-round-with-rim-000911003112000000-sku-000911002412000000/#/

Stackable, good rim, upright in dishwasher, not too heavy—the perfect plate.


### Scar treatment

Bi-Oil
https://www.bio-oil.com/en

It smells good, feels food, looks good, and seems to do its job.


### Nail hardener

alverde Nagelhärter-Kur
https://www.dm.de/alverde-naturkosmetik-nagelhaerter-kur-p4058172622793.html

It truly works, and yes, it smells good.


### Buy-it-for-life socks

Darn Tough Micro Crew Midweight Hiking Sock 
https://darntough.com/collections/best-seller/products/womens-merino-wool-hiker-micro-crew-midweight-hiking-socks

I’m not kidding, they will last forever.